# *What is _GIT_?*  ![image git](https://buddy.works/guides/covers/first-steps-with-git/first-steps-git-cover.png)
 * Version Control Software.
 * Open and Free source.
 * Track revision during development.
 * Collabs team.
  
  # *TERMINOLOGY* #
  1. ## Repository ##
  > * It can be termed as _Repo_. 
  > * These are *_Files/ Fol0ders (code files)_* that are tracked by GIT.
  2. ## GIT Lab ##
  > * Storage of GIT Repo.
  3. ## Commit ##
  > * This is similar as *_SAVING_* our work ie. files or codes.
  > * Exist in *_LOCAL MACHINE_* unless it is _Pushed_ to _Remote Repository_.
  4. ## Push ## 
  > * *_Synching commits_* to GIT lab.

  5. ## Branch ##
  > *  These are *Instances of codes* 
  > *  Master Branch is the _main software_.
 
  6. ## Merge ##
  > *  *Integrating two Branches*
  > * When the branch is Bug free it is merged to Primary codebase(Master Branch).

  7. ## Clone ##
  > * Copying Repo and paste on Local Machine.⁸

  8. ## Fork ##
  >  * To get a new Repo under your name.

  # GETTING STARTED #

  Open terminal  
  type
  > sudo apt-get install git

  # GIT WORKFLOW#

![GIT WORKFLOW IMAGE](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)


  # GIT Internals #
   there can be three main stages of files to reside ie.
  *  *MODIFIED* : 
  *   *STAGED*
  *  *COMMITED* 

  *_TREES OF SOFTWARE CODES/REPOSITORIES_*
   *  Work Space. 
   *  Staging.  
   *  Local Repository. 
   *  Remote Repository. 

# GIT COMMANDS & WORKFLOW # 
 
![IMAGE](https://www.tangowithdjango.com/book17/_images/git-sequence.svg)

1. *Cloning* 
 > $ git clone < link to repository >  
  
2. *Creating New Branch*.
 > $git checkout Master  
 > $git checkout-b < your-branch-name >
 * Modify files in working tree. 
 * Stage those changes that are Ok to staging area.
 > $ git add      
 #_to add untracked file (add all files)_.

3. *Commit*
> $git commit-sv   
#_discription   about commit_.

4. *Push*
> $ git push origin < branch name >    
#_push changes into repo._





