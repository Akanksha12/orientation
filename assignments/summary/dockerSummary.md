![what is docker](https://codespacelab.com/wp-content/uploads/2019/09/docker.jpg)

*  Its a Program for Developer to DEVELOP and RUN APPLICATIONS with Container on different enviroments.

*  Manage Dependencies.

## BASIC TERMINOLOGY

##### 1. DOCKER IMAGE:

 * Contains code ,runtime,library,enviroments variables and configuration files .It can be deploy as a container to  any enviroment.
   


![image](https://www.ubuntupit.com/wp-content/uploads/2019/11/fundamental.jpg)

* **_Working of Images_**
>* Each Image provides virtual enviroment that is shareable around the world .

> * Its a file comprised of multiple layers,that is used to execute code in docker container.

>* Images are packing part of docker analogous 
to "source code or program".

##### 2.CONTAINER:

* Running docker image .
* multiple container can be made from an image.


![container](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/container-docker-introduction/media/docker-defined/docker-container-hardware-software.png)

   * _**Working of Container**_

   >* Containers are instance of images ie use them to create an enviroment and run applications.

  > * Containers are the execution part of docker analogues to "process"


##### 3. DOCKER HUB:

![hub](https://www.javainuse.com/dock-8-11-min.JPG)

* Platform where Docker Images and Containers can be shared.
### CONTAINERS VS VIRTUAL MACHINES
![image](https://akfpartners.com//uploads/blog/VM_Image.PNG)
  * Containers provides a way to  virtualize OS so that multiple workloads can be run on single OS instances.
    
 * where as in VM the hardware is being virtualize to run multiple OS instances.

 ### DOCKER ENGINE

 ![image docker engine](https://wiki.aquasec.com/download/attachments/2854889/Docker_Engine.png)
> *  Its an opens source containerization technology that build and containerize our app .

> * Act as **Client-server application**.

> * The **Docker Engine** creates server side **Daemon** process that hosts images ,containers,networks and storage volume

> * It also provide Client side  **Command line interface** (Cli)that enables us to interact with daemon through **Docker Engine API**..

### Docker Installation 

[click here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04)

steps to install docker on ubuntu from.default repository
1.  Update software repositories.
2. uninstall old version of docker.
3. install docker.
4. Start and automate docker.
5. Check docker version ( option).


#### Basic Commands:
##### Docker ps
 ```
 $ docker ps
 container ID      Image   Command created  status   port names
577875YUGBNUI7T     ubuntu  45 mins ago   upto a min ago  elated_frankfin

 ```
 ##### Docker Start

 ```
 $ docker start 3097
 ```

 ##### Docker stop
 ```
 $ docker stop akanksha

```
###### Docker delete
```
$ docker rm
```
### Comman operations on docker

1. ##### DOWNLOAD THE DOCKER
```
$ docker pull akanksha/trydock
```

2. ##### RUN THE DOCKER WITH THIS COMMAND 
```

$ docker run -titi akanksha/trydock/bin/bash
``` 
o/p will be
root@eob547f96fo:/

3. ##### COPY THE CODE INSIDE DOCKER
* Simple python

> print ("hello python")

*  copy the file inside docker container 
```
docker cp hello.py eob547f96f8:/
```
* Write a script for installing dependencies 
requirements.sh
```
apt Update
apt install python3
```
* Copy file inside docker
```
docker cp requirements.sh eob547f96eoeeo:/
```



```
print ("hello, i am talking from container"
)
```
* copy file inside docker container
```
docker cp hello.py.eob547f96f8:/
```
This will copy hello.py into docker.

4. ##### INSTALL DEPENDENCIES  

To install additional dependency write steps of installation in shell script and run script inside container.
ex. 
* Give permission to run shell script
```
docker exec -it eob547f96fe chmod +x requirements.sh 
```
* Install dependencies
```
docker exec -t eob547f96fe://bin/bash/requirements
```

5. ##### RUN THE PROGRAM INSIDE THE DOCKER IMAGE WITH COMMIT 
```
docker start eob547f96fe:/ 
docker exec eob547f96ea:/ python3 hello.py

```

6. ##### SAVE YOUR COPIED PROGRAM INSIDE DOCKER WITH COMMIT
```
eob547f96fo akankaka/trydockttrydock
```

7. ##### PUSH DOCKER IMAGE TO THE DOCKER HUB
* tag image name with different name 
```
docker tag akanksha/trydock akanksha12/trial-dock-img
```
where akanksha12 is username at docker hub,
and trial-dock-img is 
repository 

* Push on docker hub
```
docker push username /repo
```
![whole work flow ](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)







 




